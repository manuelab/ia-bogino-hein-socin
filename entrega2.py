import itertools

from simpleai.search import CspProblem, backtrack, MOST_CONSTRAINED_VARIABLE, LEAST_CONSTRAINING_VALUE, HIGHEST_DEGREE_VARIABLE, min_conflicts

variables = ['t','E','m','l', 'e', 'L', 's', 'r', 'O', 'i', 'v', 'V', 'd', '!', 'o']

dominios = {}
for pasos in variables:
    dominios[pasos] = range(1,15)
dominios['V'] = [0]
dominios['r'] = range(1,14)
dominios['s'] = range(1,14)
dominios['o'] = range(1,7)

def antes(variables, valores, a, b):
    variable_a = variables[0]
    variable_b = variables[1]
    
    paso_a = valores[0]
    paso_b = valores[1]
    if variable_a == a and variable_b == b:
        return paso_a < paso_b
    elif variable_a == b and variable_b == a:
        return paso_a > paso_b
    else:
        return True

def sacar_manzana(variables, valores):
    return antes(variables, valores, 'e', 'L')
    
def agregar_fruta(variables, valores):
    variable_a = variables[0]
    variable_b = variables[1]

    if variable_a == 'v' or variable_b == 'v':
        return antes(variables, valores, 'O', 'v')
    elif variable_a == 'e' or variable_b == 'e':
        return antes(variables, valores, 'O', 'e')
    else:
        return True
    
def orden_encantamientos(variables, valores):
    return antes(variables, valores, 'r', 's')

def mezclar(variables, valores):
    mezclar_a = variables[0]
    mezclar_b = variables[1]
    
    paso_mezclar_a = valores[0]
    paso_mezclar_b = valores[1]

    if (mezclar_a == 'E' or mezclar_a == 'm' or mezclar_a == 'l') and (mezclar_b == 'E' or mezclar_b == 'm' or mezclar_b == 'l'):
        return (abs(paso_mezclar_a - paso_mezclar_b)) > 1
    else:
        return True

def agregar_sacudir(variables, valores):
    return antes(variables, valores, 'e', 't')
    
def sacar_sacudir(variables, valores):
    return antes(variables, valores, 't', 'L')
    
def patas(variables, valores):
    return antes(variables, valores, 'L', 'i')

def rotacion(variables, valores):
    return antes(variables, valores, 'l', 'E')

def encantamiento_naranja(variables, valores):
    return antes(variables, valores, 'v', 's')

def manzana_naranja(variables, valores):
    return antes(variables, valores, 'L', 'v')

def toque_pluma(variables, valores):
    variable_a = variables[0]
    variable_b = variables[1]

    if variable_a == 's' or variable_b == 's':
        return antes(variables, valores,  's', '!')
    elif variable_a == 'r' or variable_b == 'r':
        return antes(variables, valores,  'r', '!')
    else:
        return True

def distintos_pasos(variables, valores):
    paso_a = valores[0]
    paso_b = valores[1]
    
    return paso_a != paso_b

restricciones = []    
for paso_a, paso_b in itertools.combinations(variables, 2):
    restricciones.append(((paso_a, paso_b), sacar_manzana))
    restricciones.append(((paso_a, paso_b), agregar_fruta))
    restricciones.append(((paso_a, paso_b), orden_encantamientos))
    restricciones.append(((paso_a, paso_b), mezclar))
    restricciones.append(((paso_a, paso_b), agregar_sacudir))
    restricciones.append(((paso_a, paso_b), sacar_sacudir))
    restricciones.append(((paso_a, paso_b), patas))
    restricciones.append(((paso_a, paso_b), rotacion))
    restricciones.append(((paso_a, paso_b), encantamiento_naranja))
    restricciones.append(((paso_a, paso_b), manzana_naranja))
    restricciones.append(((paso_a, paso_b), toque_pluma))
    restricciones.append(((paso_a, paso_b), distintos_pasos))
    

def resolver(metodo_busqueda):
    if metodo_busqueda == 'backtrack':
        return globals()[metodo_busqueda](CspProblem(variables, dominios, restricciones),
                                          variable_heuristic=MOST_CONSTRAINED_VARIABLE,
                                          value_heuristic=LEAST_CONSTRAINING_VALUE,
                                          inference=True)
    elif metodo_busqueda == 'min_conflicts':
        return globals()[metodo_busqueda](CspProblem(variables, dominios, restricciones),
                                          iterations_limit= 1000)
