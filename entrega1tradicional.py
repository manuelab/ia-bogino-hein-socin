from simpleai.search import (SearchProblem, breadth_first, depth_first,
                             iterative_limited_depth_first, greedy,limited_depth_first,
                             astar)
from simpleai.search.viewers import WebViewer, ConsoleViewer, BaseViewer

def Posicion(state):
    Pos=[int(state[0]),int(state[1])]
    return Pos


def resolver(metodo_busqueda):
    viewer = BaseViewer()
    if metodo_busqueda=='breadth_first':
        result = breadth_first(Dota('04HEBE'),graph_search=True, viewer=viewer)
    
    elif metodo_busqueda=='astar':
        result = astar(Dota('04HEBE'),graph_search=True,  viewer=viewer)

    elif metodo_busqueda=='greedy':
        result = greedy(Dota('04HEBE'), graph_search=True, viewer=viewer)

    elif metodo_busqueda=='limited_depth_first':
        result = limited_depth_first(Dota('04HEBE'), graph_search=True, depth_limit=10, viewer=viewer)

    elif metodo_busqueda=='depth_first':
        result = iterative_limited_depth_first(Dota('04HEBE'), graph_search=True, viewer=viewer)

    print viewer.stats
    return result

class Dota(SearchProblem):

    def cost(self, state1, action, state2):
        return 1

    def is_goal(self, state):
        return state[2] == 'M' and state[3] == 'M'

    def actions(self, state):
        Pos= Posicion(state)
        acciones=[]
        if Pos[0]>0 and (Pos!=[3,2] or state[2]=='M'):
            acciones.append('iz')
        if Pos[0]<4 and (Pos!=[3,0] or state[3]=='M') and (Pos!=[1,2] or state[2]== 'M'):
            acciones.append('de')
        if Pos[1]>0 and (Pos!=[2,3] or state[2]=='M') and (Pos!=[4,1] or state[3]== 'M'):
            acciones.append('ar')
        if Pos[1]<4 and (Pos!=[2,1] or state[2]=='M'):
            acciones.append('ab')
        if Pos==[3,2] or Pos==[2,3] or Pos==[2,1] or Pos==[1,2] or Pos==[3,0] or Pos==[4,1]:
            acciones.append('at')

        return acciones

    def result(self, state, action):
        Pos= Posicion(state)
        PosAnt = str(Pos[0]) + str(Pos[1])
        if action=='iz':
            PosNue= str(Pos[0]-1) + str(Pos[1])
            state = state.replace(PosAnt,PosNue)
        elif action=='de':
            PosNue= str(Pos[0]+1) + str(Pos[1])
            state = state.replace(PosAnt,PosNue)
        elif action=='ar':
            PosNue= str(Pos[0]) + str(Pos[1]-1)
            state = state.replace(PosAnt,PosNue)
        elif action=='ab':
            PosNue= str(Pos[0]) + str(Pos[1]+1)
            state = state.replace(PosAnt,PosNue)
        elif action=='at':
            if Pos==[4,1] or Pos==[3,0]:
                state = state.replace('BE','M')
            else:
                state = state.replace('HE','M')
        return state
            
    def heuristic(self, state):
        Diferencia=0
        Pos= Posicion(state)
    
        if state[2]!='M':
            Diferencia += abs(Pos[0]-2)+abs(Pos[1]-2)

        if state[3]!='M':
            Diferencia += abs(Pos[0]-4)+abs(Pos[1]-0)

        return Diferencia
        
Resultado= resolver('astar')
for action, state in Resultado.path():
    print action
    print "\n".join(' '.join(map(str, row)) for row in state)


print 'costo solucion', Resultado.cost
print 'profundidad solucion', Resultado.depth

