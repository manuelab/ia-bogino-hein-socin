import random

from simpleai.search import (SearchProblem, hill_climbing, hill_climbing_stochastic,
                             hill_climbing_random_restarts, beam)
from simpleai.search.viewers import WebViewer, ConsoleViewer, BaseViewer


INITIAL = ('Assault_cuirass', 'Battlefury', 'Cloak', 'Daedalus')
items = {'Assault_cuirass':5000,
         'Battlefury':4000 ,
         'Cloak':500,
         'Daedalus':3000,
         'Lotus_orb':2500,
         'Hyperstone':2000,
         'Quelling_blade':200 ,
         'Shadow_blade':3000,
         'Veil_of_discord':2000
         }
conflictos = (('Hyperstone','Shadow_blade'),
              ('Quelling_blade','Shadow_blade'),
              ('Quelling_blade', 'Battlefury'),
              ( 'Cloak', 'Veil_of_discord'))

def resolver(metodo_busqueda):
    if metodo_busqueda == 'hill_climbing':
        return globals()[metodo_busqueda](DotaProblem(INITIAL),iterations_limit= 1000)
    elif metodo_busqueda == 'beam':
        return globals()[metodo_busqueda](DotaProblem(),beam_size = 100, iterations_limit= 1000)
    elif metodo_busqueda == 'hill_climbing_random_restarts':
        return globals()[metodo_busqueda](DotaProblem(),restarts_limit = 100, iterations_limit= 1000)
    
def valor(item):
    return items[item]

class DotaProblem(SearchProblem):

    def actions(self, state):
        acciones = []
        
        for item_viejo in state:
            for item_nuevo in items.keys():
                if item_nuevo not in state:
                    acciones.append((item_viejo, item_nuevo))
                
        return acciones


    def result(self, state, action):
        state = list(state)
        item_viejo, item_nuevo = action
        state[state.index(item_viejo)] = item_nuevo
        
        return tuple(state)


    def value(self, state):
        item1, item2, item3, item4  = list(state)
        valor_total = valor(item1)+ valor(item2)+ valor(item3) + valor(item4)
        for item_a in state:
            for item_b in state:
                if (item_a, item_b) in conflictos:
                    valor_total -= valor(item_a)+ valor(item_b)
                    
        return valor_total


    def generate_random_state(self):
        state = []
        listaitems = items.keys()
        while len(state) < 4:
            item = listaitems[random.randint(0, 8)]
            if item not in state:
                state.append(item)

        return tuple(state)

